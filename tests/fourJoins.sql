MaximumDisplayWidth 5000;

CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(1);
CALL SYSCS_UTIL.SYSCS_SET_STATISTICS_TIMING(1);

select *
from flights
inner join flightavailability on flights.flight_id = flightavailability.flight_id
inner join airlines on flights.flight_id like airlines.airline || '%'
inner join cities as c on flights.orig_airport = c.airport
inner join cities as d on flights.dest_airport = d.airport;

VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();

CALL SYSCS_UTIL.SYSCS_SET_STATISTICS_TIMING(0);
CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(0);