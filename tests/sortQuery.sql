MaximumDisplayWidth 5000;

CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(1);
CALL SYSCS_UTIL.SYSCS_SET_STATISTICS_TIMING(1);

select * from flights
where orig_airport in (
    select airport from cities
    where city_id < 10
) and dest_airport in (
    select airport from cities
    where city_id < 10
)
order by flight_id asc;

VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();

CALL SYSCS_UTIL.SYSCS_SET_STATISTICS_TIMING(0);
CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(0);