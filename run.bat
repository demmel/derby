@echo off

set derbyRunPath=jars/sane/derbyrun.jar
set command=%1

2>NUL CALL :CASE_%command%
IF ERRORLEVEL 1 CALL :DEFAULT_CASE

EXIT /B

:CASE_build
	echo "BUILD"
	call ant buildsource
	call ant buildjars
	GOTO END_CASE
:CASE_clean
	echo "CLEAN"
	call ant clobber
	rd /s /q -rf tours
	GOTO END_CASE
:CASE_setup
	echo "SETUP"
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;create=true" -jar %derbyRunPath% ij java/demo/toursdb/ToursDB_schema.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadCOUNTRIES.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadCITIES.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadAIRLINES.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadFLIGHTS1.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadFLIGHTS2.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadFLIGHTAVAILABILITY1.sql
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;" -jar %derbyRunPath% ij java/demo/toursdb/loadFLIGHTAVAILABILITY2.sql
	GOTO END_CASE
:CASE_ij
	echo "IJ"
	"%JAVA_HOME%\bin\java" -Dij.database="jdbc:derby:tours;create=true" -jar %derbyRunPath% ij
	GOTO END_CASE
:DEFAULT_CASE
	echo "HELP"
	echo "THIS IS HELP. YOU ARE WELCOME."
	echo "Run the following commands for magic to happen"
	echo ""
	echo ""
	echo "build - build derby."
	echo "clean - clean derby."
	echo "setup - creates database schema and populates tables"
	echo "ij - client to run SQL statements"
	GOTO END_CASE
:END_CASE
	VER > NUL
	GOTO :EOF