**You will need the JDK, and ant installed and in your path.**

Building
-----------
From the project root run the command

    #!bash
    ./run.sh build && ./run.sh setup

Running Without Optimizer
------------------------------------
    #!bash
    ./run.sh ijb [.sql file]

Running with Left Deep Optimizer
---------------------------------------------
    #!bash
    ./run.sh ij [.sql file]

Running with Bushy Optimizer
------------------------------------------
    #!bash
    ./run.sh ijbushy [.sql file]

Benchmarking
--------------------
To run a benchmark with one of the tests provided in the tests/ directory

    #!bash
    ./run.sh bench [iterations] [.sql file]