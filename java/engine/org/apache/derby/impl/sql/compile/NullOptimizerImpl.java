/*

   Derby - Class org.apache.derby.impl.sql.compile.OptimizerImpl

   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to you under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.apache.derby.impl.sql.compile;

import java.util.HashMap;
import org.apache.derby.iapi.error.StandardException;
import org.apache.derby.iapi.reference.SQLState;
import org.apache.derby.shared.common.sanity.SanityManager;
import org.apache.derby.iapi.sql.compile.AccessPath;
import org.apache.derby.iapi.sql.compile.CostEstimate;
import org.apache.derby.iapi.sql.compile.JoinStrategy;
import org.apache.derby.iapi.sql.compile.Optimizable;
import org.apache.derby.iapi.sql.compile.OptimizableList;
import org.apache.derby.iapi.sql.compile.OptimizablePredicateList;
import org.apache.derby.iapi.sql.compile.Optimizer;
import org.apache.derby.iapi.sql.compile.OptimizerPlan;
import org.apache.derby.iapi.sql.compile.RowOrdering;
import org.apache.derby.iapi.sql.dictionary.ConglomerateDescriptor;
import org.apache.derby.iapi.sql.dictionary.DataDictionary;
import org.apache.derby.iapi.sql.dictionary.TableDescriptor;
import org.apache.derby.iapi.util.JBitSet;
import org.apache.derby.iapi.util.StringUtil;

class NullOptimizerImpl implements Optimizer
{
    private DataDictionary			 dDictionary;
    /* The number of tables in the query as a whole.  (Size of bit maps.) */
    private int						 numTablesInQuery;
    /* The number of optimizables in the list to optimize */
    private int						 numOptimizables;

    private OptimizableList optimizableList;
    private OptimizablePredicateList predicateList;
    private JBitSet					 nonCorrelatedTableMap;

    private int[]			 proposedJoinOrder;
    private int[]					 bestJoinOrder;

    private CostEstimateImpl outermostCostEstimate;
    private CostEstimateImpl bestCost;

    private boolean 		 useStatistics;
    private int				 tableLockThreshold;

    private JoinStrategy[]	joinStrategies;

    private boolean			 foundABestPlan;

    // max memory use per table
    private int maxMemoryPerTable;

    private HashMap<Object,int[]> savedJoinOrders;

    private CostEstimate finalCostEstimate;

    NullOptimizerImpl(OptimizableList optimizableList,
                      OptimizablePredicateList predicateList,
                      DataDictionary dDictionary,
                      boolean useStatistics,
                      int maxMemoryPerTable,
                      JoinStrategy[] joinStrategies,
                      int tableLockThreshold,
                      int numTablesInQuery,
                      OptimizerPlan overridingPlan)
            throws StandardException
    {
        if (SanityManager.DEBUG) {
            SanityManager.ASSERT(optimizableList != null,
                    "optimizableList is not expected to be null");
        }

        outermostCostEstimate =  getNewCostEstimate(0.0d, 1.0d, 1.0d);

        bestCost = getNewCostEstimate(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);

        // Verify that any Properties lists for user overrides are valid
        optimizableList.verifyProperties(dDictionary);

        this.numTablesInQuery = numTablesInQuery;
        numOptimizables = optimizableList.size();
        proposedJoinOrder = new int[numOptimizables];

        bestJoinOrder = new int[numOptimizables];
        this.optimizableList = optimizableList;
        this.predicateList = predicateList;
        this.dDictionary = dDictionary;
        this.maxMemoryPerTable = maxMemoryPerTable;
        this.joinStrategies = joinStrategies;
        this.tableLockThreshold = tableLockThreshold;
        this.useStatistics = useStatistics;

		/* initialize variables for tracking permutations */
        JBitSet assignedTableMap = new JBitSet(numTablesInQuery);

		/*
		** Make a map of the non-correlated tables, which are the tables
		** in the list of Optimizables we're optimizing.  An reference
		** to a table that is not defined in the list of Optimizables
		** is presumed to be correlated.
		*/
        nonCorrelatedTableMap = new JBitSet(numTablesInQuery);
        for (int tabCtr = 0; tabCtr < numOptimizables; tabCtr++)
        {
            Optimizable	curTable = optimizableList.getOptimizable(tabCtr);
            nonCorrelatedTableMap.or(curTable.getReferencedTableMap());
        }

        savedJoinOrders = null;

        // make sure that optimizer overrides are bound and left-deep
        if ( overridingPlan != null )
        {
            if ( !overridingPlan.isBound() )
            {
                throw StandardException.newException( SQLState.LANG_UNRESOLVED_ROW_SOURCE );
            }

            int     actualRowSourceCount = optimizableList.size();
            int     overriddenRowSourceCount = overridingPlan.countLeafNodes();
            if ( actualRowSourceCount != overriddenRowSourceCount )
            {
                throw StandardException.newException
                        ( SQLState.LANG_BAD_ROW_SOURCE_COUNT, overriddenRowSourceCount, actualRowSourceCount );
            }
        }

        optimizableList.initAccessPaths(this);
        RowOrdering currentRowOrdering = new RowOrderingImpl();
        for (int i = 0; i < numOptimizables; i++) {
            proposedJoinOrder[i] = i;
            Optimizable opt = optimizableList.getOptimizable(i);
            assignedTableMap.or(opt.getReferencedTableMap());
            opt.startOptimizing(this, currentRowOrdering);
            pushPredicates(opt, assignedTableMap);
            opt.nextAccessPath(this, null, currentRowOrdering);
            opt.optimizeIt(this, predicateList, getNewCostEstimate(0.0,0.0,0.0), currentRowOrdering);
        }
        rememberBestCost(getNewCostEstimate(0.0,0.0,0.0), Optimizer.NORMAL_PLAN);
        endOfRoundCleanup();
    }

    public void prepForNextRound() {}

    public int getMaxMemoryPerTable() {
        return maxMemoryPerTable;
    }

    public boolean getNextPermutation() throws StandardException {
        return false;
    }

    private void endOfRoundCleanup() throws StandardException {
        for (int i = 0; i < numOptimizables; i++)
        {
            optimizableList.getOptimizable(i).
                    updateBestPlanMap(FromTable.REMOVE_PLAN, this);
        }
    }

    void pushPredicates(Optimizable curTable, JBitSet outerTables) throws StandardException {
        int		                numPreds        = predicateList.size();
        JBitSet	                predMap         = new JBitSet(numTablesInQuery);
        JBitSet                 curTableNums    = null;
        BaseTableNumbersVisitor btnVis          = null;
        int                     tNum;
        Predicate               pred;

        for (int predCtr = numPreds - 1; predCtr >= 0; predCtr--)
        {
            pred = (Predicate)predicateList.getOptPredicate(predCtr);

            if (pred.hasSubquery())
            {
                continue;
            }

            predMap.setTo(pred.getReferencedMap());

            for (int index = 0; index < predMap.size(); index++)
            {
                if (outerTables.get(index))
                {
                    predMap.clear(index);
                }
            }

            predMap.and(nonCorrelatedTableMap);

            boolean pushPredNow = (predMap.getFirstSetBit() == -1);

            if (pushPredNow && pred.isScopedForPush() && (numOptimizables > 1))
            {
                if (btnVis == null)
                {
                    curTableNums = new JBitSet(numTablesInQuery);
                    btnVis       = new BaseTableNumbersVisitor(curTableNums);
                }

                tNum = curTable.getTableNumber();
                curTableNums.clearAll();
                btnVis.setTableMap(curTableNums);
                curTable.accept(btnVis);
                if (tNum >= 0)
                    curTableNums.set(tNum);

                btnVis.setTableMap(predMap);
                pred.accept(btnVis);

                predMap.and(curTableNums);
                if ((predMap.getFirstSetBit() == -1))
                    pushPredNow = false;
            }

            if (pushPredNow)
            {
				/* Push the predicate and remove it from the list */
                if (curTable.pushOptPredicate(pred))
                {
                    predicateList.removeOptPredicate(predCtr);
                }
            }
        }
    }

    public boolean getNextDecoratedPermutation()
            throws StandardException
    {
        return false;
    }

    private void rememberBestCost(CostEstimate currentCost, int planType) throws StandardException {
        foundABestPlan = true;

		/* Remember the current cost as best */
        bestCost.setCost(currentCost);

        System.arraycopy(proposedJoinOrder, 0, bestJoinOrder, 0, numOptimizables);

        for (int i = 0; i < numOptimizables; i++)
        {
            optimizableList.getOptimizable(bestJoinOrder[i]).
                    rememberAsBest(planType, this);
        }
    }

    public void costPermutation() throws StandardException {}

    public void	costOptimizable(Optimizable optimizable,
                                   TableDescriptor td,
                                   ConglomerateDescriptor cd,
                                   OptimizablePredicateList predList,
                                   CostEstimate outerCost)
            throws StandardException
    {
        if(foundABestPlan) return;
        if ( ! optimizable.feasibleJoinStrategy(predList, this))
        {
            return;
        }

        CostEstimate estimatedCost = getNewCostEstimate(0.0,0.0,0.0);

        optimizable.getCurrentAccessPath().setCostEstimate(estimatedCost);

        if( ! optimizable.memoryUsageOK( estimatedCost.rowCount() / outerCost.rowCount(), maxMemoryPerTable))
        {
            return;
        }

		/* Pick the cheapest cost for this particular optimizable. */
        AccessPath ap = optimizable.getBestAccessPath();

        ap.setConglomerateDescriptor(cd);
        ap.setCostEstimate(estimatedCost);
        ap.setCoveringIndexScan(optimizable.isCoveringIndex(cd));

        ap.setNonMatchingIndexScan(
                (predList == null) ||
                        ( ! ( predList.useful(optimizable, cd) ) )
        );
        ap.setLockMode(optimizable.getCurrentAccessPath().getLockMode());
        optimizable.rememberJoinStrategyAsBest(ap);
    }

    public void	considerCost(Optimizable optimizable,
                                OptimizablePredicateList predList,
                                CostEstimate estimatedCost,
                                CostEstimate outerCost)
            throws StandardException
    {
        if ( ! optimizable.feasibleJoinStrategy(predList, this))
        {
            return;
        }

        optimizable.getCurrentAccessPath().setCostEstimate(estimatedCost);

        if( ! optimizable.memoryUsageOK( estimatedCost.rowCount() / outerCost.rowCount(),
                maxMemoryPerTable))
        {
            return;
        }

        AccessPath ap = optimizable.getBestAccessPath();
        CostEstimate bestCostEstimate = ap.getCostEstimate();

        if ((bestCostEstimate == null) ||
                bestCostEstimate.isUninitialized() ||
                (estimatedCost.compare(bestCostEstimate) <= 0))
        {
            ap.setCostEstimate(estimatedCost);
            optimizable.rememberJoinStrategyAsBest(ap);
        }
    }

    public DataDictionary getDataDictionary() {
        return dDictionary;
    }

    public void modifyAccessPaths() throws StandardException
    {
		/* Change the join order of the list of optimizables */
        optimizableList.reOrder(bestJoinOrder);

		/* Form a bit map of the tables as they are put into the join order */
        JBitSet outerTables = new JBitSet(numOptimizables);

		/* Modify the access path of each table, as necessary */
        for (int ictr = 0; ictr < numOptimizables; ictr++)
        {
            Optimizable optimizable = optimizableList.getOptimizable(ictr);

			/* Current table is treated as an outer table */
            outerTables.or(optimizable.getReferencedTableMap());
            pushPredicates(optimizable, outerTables);

            optimizableList.setOptimizable(
                    ictr,
                    optimizable.modifyAccessPath(outerTables));
        }
    }

    /** @see Optimizer#getOptimizedCost */
    public CostEstimate getOptimizedCost()
    {
        return bestCost;
    }

    public CostEstimate getFinalCost()
    {
        // If we already did this once, just return the result.
        if (finalCostEstimate != null)
            return finalCostEstimate;

        // The total cost is the sum of all the costs, but the total
        // number of rows is the number of rows returned by the innermost
        // optimizable.
        finalCostEstimate = getNewCostEstimate(0.0d, 0.0d, 0.0d);

        for (int aBestJoinOrder : bestJoinOrder) {
            CostEstimate ce = optimizableList.getOptimizable(aBestJoinOrder)
                    .getTrulyTheBestAccessPath().getCostEstimate();

            finalCostEstimate.setCost(
                    finalCostEstimate.getEstimatedCost() + ce.getEstimatedCost(),
                    ce.rowCount(),
                    ce.singleScanRowCount());
        }

        return finalCostEstimate;
    }

    /** @see Optimizer#setOuterRows */
    public void setOuterRows(double outerRows)
    {
        outermostCostEstimate.setCost(
                outermostCostEstimate.getEstimatedCost(),
                outerRows,
                outermostCostEstimate.singleScanRowCount());
    }

    /** @see Optimizer#tableLockThreshold */
    public int tableLockThreshold()
    {
        return tableLockThreshold;
    }

    /**
     * Get the number of join strategies supported by this optimizer.
     */
    public int getNumberOfJoinStrategies()
    {
        return joinStrategies.length;
    }

    /** @see Optimizer#getJoinStrategy */
    public JoinStrategy getJoinStrategy(int whichStrategy) {
        if (SanityManager.DEBUG) {
            if (whichStrategy < 0 || whichStrategy >= joinStrategies.length) {
                SanityManager.THROWASSERT("whichStrategy value " +
                        whichStrategy +
                        " out of range - should be between 0 and " +
                        (joinStrategies.length - 1));
            }

            if (joinStrategies[whichStrategy] == null) {
                SanityManager.THROWASSERT("Strategy " + whichStrategy +
                        " not filled in.");
            }
        }

        return joinStrategies[whichStrategy];
    }

    /** @see Optimizer#getJoinStrategy */
    public JoinStrategy getJoinStrategy(String whichStrategy) {
        JoinStrategy retval = null;
        String upperValue = StringUtil.SQLToUpperCase(whichStrategy);

        for (JoinStrategy joinStrategy : joinStrategies) {
            if (upperValue.equals(joinStrategy.getName())) {
                retval = joinStrategy;
            }
        }

        return retval;
    }

    /**
     @see Optimizer#uniqueJoinWithOuterTable

     @exception StandardException	Thrown on error
     */
    public double uniqueJoinWithOuterTable(OptimizablePredicateList predList) throws StandardException {
        return -1.0;
    }

    /** @see Optimizer#getLevel */
    public int getLevel() {
        return 2;
    }

    CostEstimateImpl getNewCostEstimate(double theCost,
                                        double theRowCount,
                                        double theSingleScanRowCount)
    {
        return new CostEstimateImpl(theCost, theRowCount, theSingleScanRowCount);
    }

    /** @see Optimizer#useStatistics */
    public boolean useStatistics() {
        return useStatistics && optimizableList.useStatistics();
    }

    public void updateBestPlanMaps(short action, Object planKey) throws StandardException {
        // First we process this OptimizerImpl's best join order.  If there's
        // only one optimizable in the list, then there's only one possible
        // join order, so don't bother.
        if (numOptimizables > 1)
        {
            int [] joinOrder = null;
            if (action == FromTable.REMOVE_PLAN)
            {
                if (savedJoinOrders != null)
                {
                    savedJoinOrders.remove(planKey);
                    if (savedJoinOrders.isEmpty()) {
                        savedJoinOrders = null;
                    }
                }
            }
            else if (action == FromTable.ADD_PLAN)
            {
                // If the savedJoinOrder map already exists, search for the
                // join order for the target optimizer and reuse that.
                if (savedJoinOrders == null)
                    savedJoinOrders = new HashMap<Object,int[]>();
                else
                    joinOrder = savedJoinOrders.get(planKey);

                // If we don't already have a join order array for the optimizer,
                // create a new one.
                if (joinOrder == null)
                    joinOrder = new int[numOptimizables];

                System.arraycopy(
                        bestJoinOrder, 0, joinOrder, 0, bestJoinOrder.length);

                savedJoinOrders.put(planKey, joinOrder);
            }
            else
            {
                // If we get here, we want to load the best join order from our
                // map into this OptimizerImpl's bestJoinOrder array.

                // If we don't have any join orders saved, then there's nothing to
                // load.  This can happen if the optimizer tried some join order
                // for which there was no valid plan.
                if (savedJoinOrders != null)
                {
                    joinOrder = savedJoinOrders.get(planKey);
                    if (joinOrder != null)
                    {
                        System.arraycopy(
                                joinOrder, 0, bestJoinOrder, 0, joinOrder.length);
                    }
                }
            }
        }

        // Now iterate through all Optimizables in this OptimizerImpl's
        // list and add/load/remove the best plan "mapping" for each one,
        // as described in in Optimizable.updateBestPlanMap().
        for (int i = optimizableList.size() - 1; i >= 0; i--)
        {
            optimizableList.getOptimizable(i).
                    updateBestPlanMap(action, planKey);
        }
    }

    public  int getOptimizableCount() {
        return optimizableList.size();
    }

    public  Optimizable getOptimizable( int idx ) {
        return optimizableList.getOptimizable( idx );
    }

    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }
}
