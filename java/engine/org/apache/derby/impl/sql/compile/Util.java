package org.apache.derby.impl.sql.compile;

/**
 * Created by David on 12/1/2014.
 */
public class Util {
    public static int highestPowerOfTwo(int n) {
        if (n == 0) return -1;
        return 1 << intLog2(n);
    }

    public static int intLog2(int n) {
        return 31 - Integer.numberOfLeadingZeros(n);
    }
}
