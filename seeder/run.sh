#!/bin/bash

derbyRunPath=../jars/sane/derbyrun.jar

#-Dderby.debug.true="DumpParseTree,DumpBindTree,DumpOptimizedTree"\

ij() {
    java -Dij.database="jdbc:derby:superJoin;create=true" \
		-jar $derbyRunPath ij $1
}

ijb() {
    java -Dij.database="jdbc:derby:superJoin;create=true" \
	-Dderby.optimizer.impl="org.apache.derby.impl.sql.compile.NullOptimizerImpl" \
	-jar $derbyRunPath ij $1
}

ijbushy() {
    java -Dij.database="jdbc:derby:superJoin;create=true" \
	-Dderby.optimizer.impl="org.apache.derby.impl.sql.compile.BushyOptimizerImpl" \
	-Dderby.debug.true="DumpParseTree,DumpBindTree,DumpOptimizedTree"\
	-jar $derbyRunPath ij $1
}

case "$1" in
	build)
		ant buildsource
		ant buildjars
		;;
	clean)
		ant clobber
		rm -rf superJoin
		;;
	setup)
		java -Dij.database="jdbc:derby:superJoin;create=true" -jar $derbyRunPath ij java/demo/toursdb/ToursDB_schema.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadCOUNTRIES.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadCITIES.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadAIRLINES.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadFLIGHTS1.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadFLIGHTS2.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadFLIGHTAVAILABILITY1.sql
		java -Dij.database="jdbc:derby:superJoin;" -jar $derbyRunPath ij java/demo/toursdb/loadFLIGHTAVAILABILITY2.sql
		;;
	ij)
		ij "$2"
		;;
	ijb)
		ijb "$2"
		;;
	ijbushy)
	    ijbushy "$2"
	    ;;
	bench)
		DEF_ITERS="50"
		DEF_TESTS=`find "tests" -name "*.sql"`

		ITERS=${2:-$DEF_ITERS}
		TESTS=${3:-$DEF_TESTS}

		for TEST in $TESTS; do
			echo -e "Benchmark: $TEST"
			echo -e "Null\t\tReg\t\tBushy"
			echo -e "Opt\tExec\tOpt\tExec\tOpt\tExec"
			for i in `seq "$ITERS"`; do
			    NULLOPT=`ijb "$TEST"`
			    REGOPT=`ij "$TEST"`
			    BUSHYOPT=`ijbushy "$TEST"`
				IJBO=`echo "$NULLOPT" | grep 'Optimize Time: ' | cut -c '16-'`
				IJO=`echo "$REGOPT" "$TEST" | grep 'Optimize Time: ' | cut -c '16-'`
				IJBUO=`echo "$BUSHYOPT" "$TEST" | grep 'Optimize Time: ' | cut -c '16-'`
				IJBE=`echo "$NULLOPT" | grep 'Execute Time: ' | cut -c '15-'`
				IJE=`echo "$REGOPT" | grep 'Execute Time: ' | cut -c '15-'`
				IJBUE=`echo "$BUSHYOPT" | grep 'Execute Time: ' | cut -c '15-'`
				echo -e "$IJBO\t$IJBE\t$IJO\t$IJE\t$IJBUO\t$IJBUE"
			done
			echo ""
		done
		;;
	pass)
		for TEST in `find "tests" -name "*.sql"`; do
			IJ=`ij "$TEST" | grep -B1 'ij> VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();'`
			IJB=`ijb "$TEST" | grep -B1 'ij> VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();'`
			IJBU=`ijbushy "$TEST" | grep -B1 'ij> VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();'`
			if [ "$IJ" != "$IJB" ]; then
				echo -e "$TEST Null \033[1;31mFAILED!\033[0m"
			elif [ "$IJ" != "$IJBU" ]; then
			    echo -e "$TEST Bushy \033[1;31mFAILED!\033[0m"
			else
				echo -e "$TEST \033[1;32mPASSED!\033[0m"
			fi
		done
		echo "All tests run"
		;;
	*)
		echo "THIS IS HELP. YOU ARE WELCOME."
		echo "Run the following commands for magic to happen"
		echo ""
		echo ""
		echo "build - build derby."
		echo "clean - clean derby."
		echo "setup - creates database schema and populates tables"
		echo "ij - client to run SQL statements"
		;;
esac
